﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Input;

    using PlayingCards.Core;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The card currently marked as selected or being dragged
        /// </summary>
        private ICard dragContent = null;

        /// <summary>
        /// Tells if dragging mode is active.
        /// </summary>
        private bool isDragging = false;

        /// <summary>
        /// Location where dragging started
        /// </summary>
        private Point startPosition;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow()
        {
            if (!Directory.Exists("save"))
            {
                Directory.CreateDirectory("save");
            }

            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the ViewModel that contains all the data and handles and any meta-game-logic.
        /// </summary>
        public ViewModel VM { get; set; }

        /// <summary>
        /// Prints out the about information and versions name
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void About_Click(object sender, RoutedEventArgs e)
        {
            var version = Assembly.GetEntryAssembly().GetName().Version;
            MessageBox.Show(string.Format("PlayingCards\nby David El-Saig\nVersion: {0}", version));
        }

        /// <summary>
        /// If the card is dragged onto a different card, it fires the dragged cards's Action event with the other card as targt
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Card_Drop(object sender, DragEventArgs e)
        {
            ICard target = (sender as FrameworkElement).Tag as ICard;
            if (this.dragContent == target)
            {
                return;
            }

            this.VM.Status = string.Format("{0} =>{1}", this.dragContent.Name, target.Name);

            this.OnAction(target, CardActionEventArgs.ActionDragOnOtherCard);
        }

        /// <summary>
        /// Saves the currently selected (click or drag) card.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Card_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.startPosition = e.GetPosition(null);
            this.dragContent = (sender as FrameworkElement).Tag as ICard;
        }

        /// <summary>
        /// Checks if the user is dragging something. If so, starts the drag effect.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Card_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && !this.isDragging)
            {
                var position = e.GetPosition(null);

                bool horizontal_drag = Math.Abs(position.X - this.startPosition.X)
                                       > SystemParameters.MinimumHorizontalDragDistance;
                bool vertical_drag = Math.Abs(position.Y - this.startPosition.Y)
                                     > SystemParameters.MinimumVerticalDragDistance;

                if (horizontal_drag || vertical_drag)
                {
                    this.StartDrag(e);
                }
            }
        }

        /// <summary>
        /// Displays a game end message congratulating the winner.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Game_GameOver(object sender, GameOverEventArgs e)
        {
            if ((sender as IGame).IsSinglePlayer)
            {
                MessageBox.Show("Congratulations!");
            }
            else
            {
                MessageBox.Show(string.Format("The winner is: {0}", e.Winner.Name));
            }
        }

        /// <summary>
        /// Updates the title based on Game data (title and turn count)
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Game_TurnStateChanged(object sender, TurnStateEventArgs e)
        {
            if (this.VM == null)
            {
                return;
            }

            var game = (IGame)sender;

            if (e.NewState == TurnStateEventArgs.TurnStateReady)
            {
                this.Title = string.Format("PlayingCards - {0} (turn #{1})", game.GameName, game.TurnCount);
            }
        }

        /// <summary>
        /// Starts up the game marked by this menu items.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void GamesMenu_Click(object sender, RoutedEventArgs e)
        {
            string key = ((FrameworkElement)sender).Tag as string;
            if (key != null)
            {
                this.StartGame(key);
            }
        }

        /// <summary>
        /// Activates the CardVisual associated with this menu item. Select before starting the game!
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void GraphicsItem_Click(object sender, RoutedEventArgs e)
        {
            var visual = (sender as FrameworkElement).DataContext as ICardVisual;
            if (visual != null)
            {
                this.VM.CardVisual = visual;
            }
        }

        /// <summary>
        /// Displays the high scores table associated with this game.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void HighScores_Click(object sender, RoutedEventArgs e)
        {
            // var temp = new System.ComponentModel.BindingList<HighScore>();
            // temp.Add(new HighScore(new Core.Player("test1", 10), DateTime.Now - VM.StartTime));
            // temp.Add(new HighScore(new Core.Player("test2", 9), DateTime.Now - VM.StartTime));
            // new HighScoresWindow(temp).ShowDialog();
            new HighScoresWindow(this.VM.HighScores).ShowDialog();
        }

        /// <summary>
        /// Calls the current game's Deserialise method in the save folder if the {game type name}.sav file exists.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (this.VM.Game == null)
            {
                return;
            }

            string path = Path.Combine("save", this.VM.Game.GetType().Name + ".sav");

            if (!File.Exists(path))
            {
                this.VM.Status = "No save file exists.";
            }
            else
            {
                this.VM.Game = this.VM.Game.Deserialize(path);
            }
        }

        /// <summary>
        /// Safely triggers the card's Action event through its OnAction method. It also catches any CardActionException and writes the message to the status bar.
        /// </summary>
        /// <param name="target">The event's destination.</param>
        /// <param name="action">The name of the action type being performed.</param>
        /// <param name="action_data">Additional action data. (ex. target pile)</param>
        /// <param name="card">The selected card or this.DragContent if it's null</param>
        private void OnAction(
            ICard target,
            string action,
            Dictionary<string, object> action_data = null,
            ICard card = null)
        {
            if (card == null)
            {
                card = this.dragContent;
            }

            try
            {
                card.OnAction(new CardActionEventArgs(target, action, action_data));
            }
            catch (CardActionException ex)
            {
                this.VM.Status = ex.Message;
            }
        }

        /// <summary>
        /// Looks at the target pile the drag and drop ended on. If it's empty, then fires an Action event on the dragged card with it.
        /// Otherwise it acts as if Card_Drop was called on the top card of that pile.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Pile_Drop(object sender, DragEventArgs e)
        {
            IPile target = (sender as FrameworkElement).DataContext as IPile;

            if (target == null)
            {
                return;
            }

            this.VM.Status = string.Format("{0} =>{1}", this.dragContent.Name, target.Name);

            var action_data = new Dictionary<string, object>();
            action_data[CardActionEventArgs.DataPileTo] = target;

            this.OnAction(null, CardActionEventArgs.ActionDragOnEmptyPile, action_data);
        }

        /// <summary>
        /// Calls the current game's Serialise method in the save folder.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (this.VM.Game == null)
            {
                return;
            }

            string path = Path.Combine("save", this.VM.Game.GetType().Name + ".sav");
            this.VM.Game.Serialize(path);
        }

        /// <summary>
        /// Starts up the drag effect
        /// </summary>
        /// <param name="e">Event Data</param>
        private void StartDrag(MouseEventArgs e)
        {
            this.isDragging = true;

            DataObject data = new DataObject(DataFormats.Text, this.dragContent.Name);
            DragDropEffects de = DragDrop.DoDragDrop(this.Game, data, DragDropEffects.Move);

            this.isDragging = false;
        }

        /// <summary>
        /// Creates a new instance of the game (using game_key on the ViewModel.GameConstructors dictionary), attaches all game events defined in this class and starts the game.
        /// </summary>
        /// <param name="game_key">The name used in the GameConstructors dictionary.</param>
        /// <returns>The initialised game that is also available in VM.Game by the end of this call.</returns>
        private IGame StartGame(string game_key)
        {
            IGame game = ViewModel.GameConstructors[game_key]();
            game.TurnStateChanged += this.Game_TurnStateChanged;
            game.GameOver += this.Game_GameOver;
            this.VM.Game = game;
            game.Initialize();

            return game;
        }

        /// <summary>
        /// Triggers an Action event on the selected card using the now right-clicked card or pile as target.
        /// Alternative to drag and drop, accessibility feature.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void TreeViewItem_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.dragContent == null)
            {
                return;
            }

            object content = (sender as FrameworkElement).DataContext;
            ICard card = content as ICard;
            IPile pile = content as IPile;

            if (pile != null)
            {
                if (pile.Cards.Count(x => x != null) == 0)
                {
                    var action_data = new Dictionary<string, object>();
                    action_data[CardActionEventArgs.DataPileTo] = pile;

                    this.OnAction(null, CardActionEventArgs.ActionDragOnEmptyPile, action_data);
                    return;
                }
                else
                {
                    card = pile.Cards.Last();
                }
            }

            if (card != null)
            {
                this.OnAction(card, CardActionEventArgs.ActionDragOnOtherCard);
                return;
            }
        }

        /// <summary>
        /// Creates the ViewModel and links the Game and Graphics menus to their respective static sources.
        /// </summary>
        /// <param name="sender">The Framework Element that triggers this event.</param>
        /// <param name="e">Event Data</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.VM = new ViewModel(null) { CardVisual = ViewModel.CardVisuals["FrenchCard"] };
            this.DataContext = this.VM;

            this.GamesMenu.ItemsSource = ViewModel.GameConstructors.ToList();
            this.GraphicsMenu.ItemsSource = ViewModel.CardVisuals.Values;

            // var ser = new ExtendedXmlSerialization.ExtendedXmlSerializer();
            // var freecell = VM.Game as FreeCellPlugin.FreeCellGame;
        }

        /// <summary>
        /// Clicking on this will set the game state to Core.TurnStateEventArgs.TurnStateGameOver.
        /// </summary>
        /// <param name="sender">Sender is unused.</param>
        /// <param name="e">Event Data</param>
        private void CheatVictory_Click(object sender, RoutedEventArgs e)
        {
            if (this.VM.Game == null)
            {
                return;
            }

            this.VM.Game.TurnState = Core.TurnStateEventArgs.TurnStateGameOver;
            this.VM.Game.OnGameOver();
        }
    }
}