﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighScore.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    using PlayingCards.Core;

    /// <summary>
    /// The high score.
    /// </summary>
    public class HighScore : INotifyPropertyChanged
    {
        /// <summary>
        /// The game duration.
        /// </summary>
        private TimeSpan duration;

        /// <summary>
        /// The winner.
        /// </summary>
        private Player player;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScore"/> class.
        /// </summary>
        public HighScore()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScore"/> class.
        /// </summary>
        /// <param name="player">
        /// The winner.
        /// </param>
        /// <param name="duration">
        /// The game duration.
        /// </param>
        public HighScore(Player player, TimeSpan duration)
        {
            this.Player = player;
            this.Duration = duration;
        }

        /// <summary>
        /// PropertyChanged event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the duration of the game.
        /// </summary>
        [XmlIgnore]
        public TimeSpan Duration
        {
            get
            {
                return this.duration;
            }

            set
            {
                if (this.duration != value)
                {
                    this.PropertyChanged.Update(this, ref this.duration, value);
                    this.PropertyChanged.Notify(this, nameof(this.DurationMS));
                }
            }
        }

        /// <summary>
        /// Gets or sets the game duration in milliseconds
        /// </summary>
        public int DurationMS
        {
            get
            {
                return (int)this.Duration.TotalMilliseconds;
            }

            set
            {
                this.Duration = new TimeSpan(0, 0, 0, 0, value);
            }
        }

        /// <summary>
        /// Gets or sets the winner.
        /// </summary>
        public Player Player
        {
            get
            {
                return this.player;
            }

            set
            {
                if (this.player != value)
                {
                    this.PropertyChanged.Update(this, ref this.player, value);
                }
            }
        }
    }
}