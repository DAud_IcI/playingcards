﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewModel.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml.Serialization;

    using PlayingCards.Core;

    /// <summary>
    /// The view-model that contains all non-WPF-related app components
    /// </summary>
    public class ViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The plugin file name filter.
        /// </summary>
        private const string PluginFile = "*Plugin.dll";

        /// <summary>
        /// Contains the loaded assemblies.
        /// </summary>
        private static List<Assembly> loadedAssemblies;

        /// <summary>
        /// The selected CardVisual.
        /// </summary>
        private ICardVisual cardVisual;

        /// <summary>
        /// The selected IGame.
        /// </summary>
        private IGame game;

        /// <summary>
        /// Start time of the current games.
        /// </summary>
        private DateTime startTime;

        /// <summary>
        /// The status bar text.
        /// </summary>
        private string status = "Ready...";

        /// <summary>
        /// Initializes static members of the <see cref="ViewModel"/> class.
        /// </summary>
        static ViewModel()
        {
            LoadPluginAssemblies();

            CardVisuals =
                GetConstructors<ICardVisual>()
                    .Select(constructor => (ICardVisual)constructor.Invoke(Type.EmptyTypes))
                    .ToDictionary(instance => instance.Name);

            GameConstructors = GetConstructors<IGame>()
                .ToDictionary(
                    constructor => constructor.DeclaringType.Name,
                    constructor => new Func<IGame>(() => (IGame)constructor.Invoke(Type.EmptyTypes)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        /// <param name="game">
        /// The current game.
        /// </param>
        public ViewModel(IGame game)
        {
            this.Game = game;
            Instance = this;
        }

        /// <summary>
        /// PropertyChanged event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets a map of the CardVisuals according to their name.
        /// </summary>
        public static Dictionary<string, ICardVisual> CardVisuals { get; private set; }

        /// <summary>
        /// Gets a map of the IGame constructors according to their class name.
        /// </summary>
        public static Dictionary<string, Func<IGame>> GameConstructors { get; private set; }

        /// <summary>
        /// Gets the latest ViewModel instance.
        /// </summary>
        public static ViewModel Instance { get; private set; }

        /// <summary>
        /// Gets or sets the current CardVisual.
        /// </summary>
        public ICardVisual CardVisual
        {
            get
            {
                return this.cardVisual;
            }

            set
            {
                if (this.cardVisual != value)
                {
                    this.PropertyChanged.Update(this, ref this.cardVisual, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current IGame.
        /// </summary>
        public IGame Game
        {
            get
            {
                return this.game;
            }

            set
            {
                if (this.game == value)
                {
                    return;
                }

                this.PropertyChanged.Update(this, ref this.game, value);

                if (value == null)
                {
                    return;
                }

                value.GameOver += this.Game_GameOver;

                this.HighScores = new BindingList<HighScore>();
                string score_path = Path.Combine("HighScores", this.Game.GetType().Name + ".scores.xml");
                if (File.Exists(score_path))
                {
                    var ser = new XmlSerializer(typeof(HighScore[]));
                    using (var sr = new StreamReader(score_path, Encoding.UTF8))
                    {
                        foreach (var score in (HighScore[])ser.Deserialize(sr))
                        {
                            this.HighScores.Add(score);
                        }
                    }
                }

                this.HighScores.ListChanged += this.HighScores_ListChanged;

                this.StartTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Gets or sets the high scores.
        /// </summary>
        public BindingList<HighScore> HighScores { get; set; }

        /// <summary>
        /// Gets or sets the game start time.
        /// </summary>
        public DateTime StartTime
        {
            get
            {
                return this.startTime;
            }

            set
            {
                if (this.startTime != value)
                {
                    this.PropertyChanged.Update(this, ref this.startTime, value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the status bar message.
        /// </summary>
        public string Status
        {
            get
            {
                return this.status;
            }

            set
            {
                if (this.status != value)
                {
                    this.PropertyChanged.Update(this, ref this.status, value);
                }
            }
        }

        /// <summary>
        /// Gets the constructors that can be casted to T
        /// </summary>
        /// <typeparam name="T">Type to be filtered on</typeparam>
        /// <returns>the constructors</returns>
        private static IEnumerable<ConstructorInfo> GetConstructors<T>()
        {
            return
                loadedAssemblies.SelectMany(assembly => assembly.GetTypes())
                    .Where(type => typeof(T).IsAssignableFrom(type) && !type.IsAbstract)
                    .Select(type => type.GetConstructor(Type.EmptyTypes))
                    .Where(constructor => constructor != null);
        }

        /// <summary>
        /// Load assemblies that represent a plugin (according to PluginFile filter) in the base directory or the plugins subdirectory
        /// </summary>
        private static void LoadPluginAssemblies()
        {
            loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            string[] loaded_paths = loadedAssemblies.Select(a => a.Location).ToArray();

            string base_dir = AppDomain.CurrentDomain.BaseDirectory;
            IEnumerable<string> referenced_paths = Directory.GetFiles(base_dir, PluginFile);
            string plugin_dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "plugins");
            if (Directory.Exists(plugin_dir))
            {
                referenced_paths = referenced_paths.Union(Directory.GetFiles(plugin_dir, PluginFile));
            }

            foreach (string path in
                referenced_paths.Where(r => !loaded_paths.Contains(r, StringComparer.InvariantCultureIgnoreCase)))
            {
                loadedAssemblies.Add(AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(path)));
            }
        }

        /// <summary>
        /// Saves the high score and turns off the game when it's over.
        /// </summary>
        /// <param name="sender">sender is not used.</param>
        /// <param name="e">game over data, contains the winner</param>
        private void Game_GameOver(object sender, GameOverEventArgs e)
        {
            this.HighScores.Add(new HighScore((Player)e.Winner, DateTime.Now - this.StartTime));
            this.Game = null;
        }

        /// <summary>
        /// Ensures that only 10 or less items are in the high scores and saves it to HighScores directory.
        /// </summary>
        /// <param name="sender">sender is not used.</param>
        /// <param name="e">e is not used.</param>
        private void HighScores_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (this.HighScores.Count <= 10)
            {
                var ser = new XmlSerializer(typeof(HighScore[]));
                if (!Directory.Exists("HighScores"))
                {
                    Directory.CreateDirectory("HighScores");
                }

                using (
                    var sw = new StreamWriter(
                                 Path.Combine("HighScores", this.Game.GetType().Name + ".scores.xml"),
                                 false,
                                 Encoding.UTF8))
                {
                    ser.Serialize(sw, this.HighScores.ToArray());
                }

                return;
            }

            this.HighScores.Remove(
                this.HighScores.OrderByDescending(x => x.Player.Score).ThenBy(x => x.Duration).Last());
        }
    }
}