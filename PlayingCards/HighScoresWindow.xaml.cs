﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighScoresWindow.xaml.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;

    /// <summary>
    /// Interaction logic for HighScoresWindow.xaml
    /// </summary>
    public partial class HighScoresWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoresWindow"/> class.
        /// </summary>
        /// <param name="scores">
        /// The high scores.
        /// </param>
        public HighScoresWindow(IEnumerable<HighScore> scores)
        {
            this.Scores = scores.OrderByDescending(x => x.Player.Score).ThenBy(x => x.Duration).ToList();

            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the high scores.
        /// </summary>
        public List<HighScore> Scores { get; set; }

        /// <summary>
        /// Closes the application when Ok is pressed
        /// </summary>
        /// <param name="sender">The ok button</param>
        /// <param name="e">event args</param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}