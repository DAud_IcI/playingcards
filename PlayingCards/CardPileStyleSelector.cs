﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardPileStyleSelector.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Selects style based on whether the item is a Core.ICard or Core.IPile.
    /// </summary>
    public class CardPileStyleSelector : StyleSelector
    {
        /// <summary>
        /// Gets or sets the card style.
        /// </summary>
        public Style Card { get; set; }

        /// <summary>
        /// Gets or sets the pile style.
        /// </summary>
        public Style Pile { get; set; }

        /// <summary>
        /// Selects style based on whether the item is a Core.ICard or Core.IPile.
        /// </summary>
        /// <param name="item">The bound object.</param>
        /// <param name="container">container is not used.</param>
        /// <returns>The style to be used for this FrameWorkElement</returns>
        public override Style SelectStyle(object item, DependencyObject container)
        {
            var data = item;

            if (data is Core.ICard)
            {
                return this.Card;
            }

            return this.Pile;
        }
    }
}