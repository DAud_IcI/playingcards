﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}