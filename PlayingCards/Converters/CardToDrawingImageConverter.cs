﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardToDrawingImageConverter.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   Defines the CardToDrawingImageConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// ICard to DrawingImage converter.
    /// </summary>
    public class CardToDrawingImageConverter : IValueConverter
    {
        /// <summary>
        /// Converts ICard to DrawingImage via the currently selected CardVisual instance.
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>A DrawingImage</returns>
        public object Convert(object value, Type target_type, object parameter, CultureInfo culture)
        {
            return ViewModel.Instance.CardVisual.DrawCard((Core.ICard)value);
        }

        /// <summary>
        /// ConvertBack is not supported
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>Throws NotImplementedException.</returns>
        public object ConvertBack(object value, Type target_type, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}