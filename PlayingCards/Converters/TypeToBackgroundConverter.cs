﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeToBackgroundConverter.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   Defines the CardToDrawingImageConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Converts object type to a background based on the static type-color map.
    /// </summary>
    public class TypeToBackgroundConverter : IValueConverter
    {
        /// <summary>
        /// Initializes static members of the <see cref="TypeToBackgroundConverter"/> class.
        /// </summary>
        static TypeToBackgroundConverter()
        {
            NullColour = Colors.Black;

            TypeColours = new Dictionary<Type, Color>();
            TypeColours[typeof(Core.IPile)] = Colors.Green;
            TypeColours[typeof(Core.ICard)] = Colors.White;

            NamedColours = new Dictionary<string, Color>();
            NamedColours["FrenchCard"] = Colors.WhiteSmoke;
        }

        /// <summary>
        /// Gets or sets the named colours.
        /// </summary>
        public static Dictionary<string, Color> NamedColours { get; set; }

        /// <summary>
        /// Gets or sets the null colour.
        /// </summary>
        public static Color NullColour { get; set; }

        /// <summary>
        /// Gets or sets the colours based on Type.
        /// </summary>
        public static Dictionary<Type, Color> TypeColours { get; set; }

        /// <summary>
        /// Converts object type to a background based on the static type-color map.
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>The Color according to TypeToBackgroundConverter.TypeColours</returns>
        public object Convert(object value, Type target_type, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return new SolidColorBrush(NullColour);
            }

            var card = value as Core.ICard;
            if (card != null)
            {
                Color ret = NullColour;
                if (card.FaceUp)
                {
                    ret = TypeColours[typeof(Core.ICard)];
                    if (!string.IsNullOrWhiteSpace(card.FaceImage))
                    {
                        if (card.FaceImage[0] == '#')
                        {
                            ret = (Color)ColorConverter.ConvertFromString(card.FaceImage);
                        }
                        else if (card.FaceImage[0] == ':')
                        {
                            ret = (Color)ColorConverter.ConvertFromString(card.FaceImage.Substring(1));
                        }
                        else if (card.FaceImage[0] == '!')
                        {
                            ret = NamedColours[card.FaceImage.Substring(1)];
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(card.FaceImage))
                    {
                        if (card.BackImage[0] == '#')
                        {
                            ret = (Color)ColorConverter.ConvertFromString(card.BackImage);
                        }
                        else if (card.BackImage[0] == ':')
                        {
                            ret = (Color)ColorConverter.ConvertFromString(card.BackImage.Substring(1));
                        }
                        else if (card.BackImage[0] == '!')
                        {
                            ret = NamedColours[card.BackImage.Substring(1)];
                        }
                    }
                }

                return new SolidColorBrush(ret);
            }

            Type key = null;

            foreach (var type in TypeColours.Keys)
            {
                if (key != null && key.IsSubclassOf(type))
                {
                    continue; // the more specific type takes preference
                }

                if (type.IsInstanceOfType(value))
                {
                    key = type;
                }
            }

            return new SolidColorBrush(key == null ? NullColour : TypeColours[key]);
        }

        /// <summary>
        /// ConvertBack is not supported
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>Throws NotImplementedException.</returns>
        public object ConvertBack(object value, Type target_type, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}