﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PileToMarginConverter.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   Defines the CardToDrawingImageConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Windows;
    using System.Windows.Data;

    using PlayingCards.Core;

    /// <summary>
    /// Converts a Core.IPile or Core.ICard to appropriate Margin in order to facilitate Grid style layouting placement.
    /// </summary>
    public class PileToMarginConverter : IMultiValueConverter
    {
        /// <summary>
        /// Gets the thickness of the pile or card based on placement.
        /// </summary>
        /// <param name="actual_width">container's ActualWidth</param>
        /// <param name="actual_height">container's ActualHeight</param>
        /// <param name="aspect_ratio">expected card aspect ratio from the specified card or from any in the pile of the layout</param>
        /// <param name="pile">The bound IPile or the container pile of the bound ICard</param>
        /// <param name="card">The bound ICard or null</param>
        /// <returns>the calculated thickness</returns>
        public static Thickness CalculateThickness(
            double actual_width,
            double actual_height,
            double aspect_ratio,
            IPile pile,
            ICard card)
        {
            int piles_horizontal = ViewModel.Instance.Game.Layout.Max(x => x.Left) + 1;
            int piles_vertical = ViewModel.Instance.Game.Layout.Max(y => y.Top) + 1;

            double width = actual_width / piles_horizontal;
            double height = actual_height / piles_vertical;

            double left = pile.Left * width;
            double top = pile.Top * height;

            if (height < width / aspect_ratio)
            {
                double new_width = height * aspect_ratio;
                left += (width - new_width) / 2;
                width = new_width;
            }
            else if (width < height * aspect_ratio)
            {
                double new_height = height * aspect_ratio;

                // top += (height - new_height) / 2;
                height = new_height;
            }

            if (card != null)
            {
                left = 0;
                switch (pile.Layout)
                {
                    case Core.PileLayout.Tableau:
                        top = ViewModel.Instance.Game.GetPile(card).Cards.IndexOf(card) * height * 0.12;
                        break;
                    default:
                        top = 0;
                        break;
                }
            }
            else
            {
                top = pile.Top * height;
            }

            double right = actual_width - left - width;
            double bottom = actual_height - top - height;

            if (card != null)
            {
                right = bottom = 0;
            }
            else
            {
                switch (pile.Layout)
                {
                    case Core.PileLayout.Tableau:
                        bottom -= (pile.Cards.Count - 1) * height * 0.12;
                        break;
                    default:
                        top = 0;
                        break;
                }
            }

            return new Thickness(left, top, right, bottom);
        }

        /// <summary>
        /// Converts a Core.IPile or Core.ICard to appropriate Margin in order to facilitate Grid style layouting placement.
        /// </summary>
        /// <param name="values">The bound values.</param>
        /// <param name="target_types">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>A Margin that describes the card's placement.</returns>
        public object Convert(object[] values, Type target_types, object parameter, CultureInfo culture)
        {
            IPile pile = null;
            ICard card = null;
            FrameworkElement container = null;

            foreach (object value in values)
            {
                if (value is Core.IPile)
                {
                    pile = (Core.IPile)value;
                }
                else if (value is Core.ICard)
                {
                    card = (Core.ICard)value;
                }
                else if (value is FrameworkElement)
                {
                    container = (FrameworkElement)value;
                }
            }

            if (card != null)
            {
                pile = ViewModel.Instance.Game.GetPile(card);
            }

            if (pile == null)
            {
                throw new Exception("None of the bound values are IPile or ICard");
            }

            if (container == null)
            {
                throw new Exception("None of the bound values are FrameworkElement");
            }

            bool aspect_ratio_set = false;
            double aspect_ratio = 1;
            if (card != null)
            {
                aspect_ratio = card.AspectRatio;
                aspect_ratio_set = true;
            }

            if (pile.Cards != null)
            {
                foreach (var c in pile.Cards)
                {
                    aspect_ratio_set = true;
                    aspect_ratio = c.AspectRatio;
                    break;
                }
            }

            if (!aspect_ratio_set)
            {
                foreach (var p in ViewModel.Instance.Game.Layout)
                {
                    if (p.Cards != null)
                    {
                        foreach (var c in p.Cards)
                        {
                            aspect_ratio_set = true;
                            aspect_ratio = c.AspectRatio;
                            break;
                        }
                    }
                }
            }

            if (!aspect_ratio_set)
            {
                throw new Exception("No cards are in the game!");
            }

            return CalculateThickness(
                container.ActualWidth,
                container.ActualHeight,
                aspect_ratio,
                pile,
                card);
        }

        /// <summary>
        /// ConvertBack is not supported
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_types">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>Throws NotImplementedException.</returns>
        public object[] ConvertBack(object value, Type[] target_types, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}