﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotNullToVisibilityConverter.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   Defines the CardToDrawingImageConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    /// <summary>
    /// Converts the input to Visibility.Visible if it's not null and to Visibility.Hidden otherwise.
    /// </summary>
    public class NotNullToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Converts the input to Visibility.Visible if it's not null and to Visibility.Hidden otherwise.
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">Set it to "collapse" to get Visibility.Collapse instead of Visibility.Hidden on null.</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>Visibility.Visible or Visibility.Hidden</returns>
        public object Convert(object value, Type target_type, object parameter, CultureInfo culture)
        {
            Visibility hidden = parameter != null && parameter.ToString() == "collapse" ? Visibility.Collapsed : Visibility.Hidden;
            return value != null ? Visibility.Visible : hidden;
        }

        /// <summary>
        /// ConvertBack is not supported
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>Throws NotImplementedException.</returns>
        public object ConvertBack(object value, Type target_type, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}