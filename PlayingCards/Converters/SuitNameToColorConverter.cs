﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SuitNameToColorConverter.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   Defines the CardToDrawingImageConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;
    
    /// <summary>
    /// Converts the input text to red or black based on the first characer
    /// </summary>
    public class SuitNameToColorConverter : IValueConverter
    {
        /// <summary>
        /// Initializes static members of the <see cref="SuitNameToColorConverter"/> class. 
        /// </summary>
        static SuitNameToColorConverter()
        {
            ColourMap = new Dictionary<Core.Colour, Color>();
            ColourMap[Core.Colour.Red] = Colors.DarkRed;
            ColourMap[Core.Colour.Black] = Colors.Black;
        }

        /// <summary>
        /// Gets or sets the colour map that assigns a specific colour to the abstrcat Core.Colour enumeration.
        /// </summary>
        public static Dictionary<Core.Colour, Color> ColourMap { get; set; }

        /// <summary>
        /// Converts the input text to red or black based on the first characer
        /// </summary>
        /// <param name="text">The bound value.</param>
        /// <returns>SolidColorBrush : red or black as defined in the static variables</returns>
        public static SolidColorBrush Convert(string text)
        {
            SolidColorBrush ret = SystemColors.ActiveCaptionBrush;

            if (!string.IsNullOrWhiteSpace(text) && Core.Suits.IsSuit(text[0]))
            {
                ret = new SolidColorBrush(ColourMap[Core.Suits.GetColour(Core.Suits.Enums[text[0]])]);
            }

            return ret;
        }

        /// <summary>
        /// Converts the input text to red or black based on the first characer
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>SolidColorBrush : red or black as defined in the static ColourMap</returns>
        public object Convert(object value, Type target_type, object parameter, CultureInfo culture)
        {
            return Convert(value.ToString());
        }

        /// <summary>
        /// ConvertBack is not supported
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_type">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>Throws NotImplementedException.</returns>
        public object ConvertBack(object value, Type target_type, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}