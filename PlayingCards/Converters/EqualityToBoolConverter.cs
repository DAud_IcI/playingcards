﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EqualityToBoolConverter.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   Defines the CardToDrawingImageConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    /// <summary>
    /// Multiconverter that checks if its bound parameters are equal
    /// </summary>
    public class EqualityToBoolConverter : IMultiValueConverter
    {
        /// <summary>
        /// Checks if the inputs are equal
        /// </summary>
        /// <param name="values">The bound values.</param>
        /// <param name="target_types">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>A boolean value</returns>
        public object Convert(object[] values, Type target_types, object parameter, CultureInfo culture)
        {
            if (values.Length < 1)
            {
                return false;
            }

            object test = values[0];
            for (int i = 1; i < values.Length; i++)
            {
                if (values[i] != test)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// ConvertBack is not supported
        /// </summary>
        /// <param name="value">The bound value.</param>
        /// <param name="target_types">targetType is ignored.</param>
        /// <param name="parameter">parameter is ignored</param>
        /// <param name="culture">culture is ignored</param>
        /// <returns>Throws NotImplementedException.</returns>
        public object[] ConvertBack(object value, Type[] target_types, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}