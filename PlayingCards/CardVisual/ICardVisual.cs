﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICardVisual.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards
{
    using System.Windows.Media;

    using PlayingCards.Core;

    /// <summary>
    /// The ICardVisual interface.
    /// It defines the graphics module.
    /// </summary>
    public interface ICardVisual
    {
        /// <summary>
        /// Gets the identifying name of the instance.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// When defined by its overriding class it displays the card as an image.
        /// </summary>
        /// <param name="card">The card to be displayed</param>
        /// <returns>An image to serve as the face of the card</returns>
        DrawingImage DrawCard(ICard card);
    }
}