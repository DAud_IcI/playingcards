﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardVisualBase.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System.Globalization;
    using System.Windows;
    using System.Windows.Media;

    using PlayingCards.Core;

    /// <summary>
    /// Base implementation of the ICardVisual class
    /// </summary>
    public abstract class CardVisualBase : ICardVisual
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardVisualBase"/> class. 
        /// </summary>
        /// <param name="name">The name of the CardVisual</param>
        protected CardVisualBase(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        public virtual string Name { get; private set; }

        /// <summary>
        /// Turns the provided text into a path centred within the area of specified size.
        /// </summary>
        /// <param name="name">The text ot be displayed</param>
        /// <param name="width">Width of the area</param>
        /// <param name="height">Height of the area</param>
        /// <param name="font_size">Font size of the text</param>
        /// <param name="typeface">Font name of the text</param>
        /// <returns>The text properly centred and turned into a path geometry</returns>
        public static PathGeometry TextToCentredPath(
            string name,
            double width,
            double height,
            int font_size,
            string typeface = "Segoe UI")
        {
            var text = new FormattedText(
                           name,
                           CultureInfo.CurrentCulture,
                           FlowDirection.LeftToRight,
                           new Typeface(typeface),
                           font_size,
                           Brushes.Black);
            text.TextAlignment = TextAlignment.Center;
            var text_geometry = text.BuildGeometry(new Point(0, 0)).GetFlattenedPathGeometry();
            text_geometry.Transform =
                new TranslateTransform(
                    (width / 2) - text_geometry.Bounds.TopLeft.X - (text_geometry.Bounds.Width / 2),
                    (height / 2) - text_geometry.Bounds.TopLeft.Y - (text_geometry.Bounds.Height / 2));

            return text_geometry;
        }

        /// <summary>
        /// When defined by its overriding class it displays the card as an image.
        /// </summary>
        /// <param name="card">The card to be displayed</param>
        /// <returns>An image to serve as the face of the card</returns>
        public abstract DrawingImage DrawCard(ICard card); // { throw new NotImplementedException(); }
    }
}