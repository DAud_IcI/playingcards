﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardVisualFrench.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards
{
    using System.Windows;
    using System.Windows.Media;

    using PlayingCards.Core;

    /// <summary>
    /// The card visual french.
    /// </summary>
    public class CardVisualFrench : CardVisualBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardVisualFrench"/> class.
        /// </summary>
        public CardVisualFrench()
            : base("FrenchCard")
        {
        }

        /// <summary>
        /// Displays the card as an image
        /// </summary>
        /// <param name="card">The card to be displayed</param>
        /// <returns>An image to serve as the face of the card</returns>
        public override DrawingImage DrawCard(ICard card)
        {
            double w = 100;
            double h = 100 / card.AspectRatio;
            var colour = SuitNameToColorConverter.Convert(card.Name);

            var group = new DrawingGroup();
            group.Children.Add(
                new GeometryDrawing
                    {
                        Brush = Brushes.Transparent,
                        Geometry = new RectangleGeometry(new Rect(0, 0, w, h))
                    });

            // group.Children.Add(new GeometryDrawing { Brush = colour, Geometry = new EllipseGeometry(new Point(w / 2, h / 2), 20, 20) });
            group.Children.Add(
                new GeometryDrawing { Brush = colour, Geometry = TextToCentredPath(card.Name[0].ToString(), w, h, 80) });
            group.Children.Add(
                new GeometryDrawing
                    {
                        Brush = Brushes.White,
                        Geometry = TextToCentredPath(card.Name.Substring(1), w, h, 38)
                    });

            return new DrawingImage { Drawing = group };
        }
    }
}