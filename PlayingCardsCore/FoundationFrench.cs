﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FoundationFrench.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The foundation (using French cards only).
    /// </summary>
    public class FoundationFrench : Pile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FoundationFrench"/> class.
        /// </summary>
        public FoundationFrench()
            : base(PileLayout.Foundation)
        {
            this.AllowedSuit = Suit.Unknown;
            this.BindingListCards.ListChanged += this.CheckSuit;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FoundationFrench"/> class.
        /// </summary>
        /// <param name="suit">
        /// The suit.
        /// </param>
        /// <param name="top">
        /// The top.
        /// </param>
        /// <param name="left">
        /// The left.
        /// </param>
        public FoundationFrench(Suit suit, int top, int left)
            : this(suit, Suits.Chars[suit].ToString(), top, left)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FoundationFrench"/> class.
        /// </summary>
        /// <param name="suit">
        /// The suit.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="top">
        /// The top.
        /// </param>
        /// <param name="left">
        /// The left.
        /// </param>
        public FoundationFrench(Suit suit, string name, int top, int left)
            : base(name, top, left, PileLayout.Foundation)
        {
            this.AllowedSuit = suit;
            this.BindingListCards.ListChanged += this.CheckSuit;
        }

        /// <summary>
        /// Gets or sets the allowed suit.
        /// </summary>
        public Suit AllowedSuit { get; set; }

        /// <summary>
        /// Gets the top card.
        /// </summary>
        public FrenchCard TopCard { get; private set; }

        /// <summary>
        /// Checks the suit.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        /// <exception cref="ArgumentException">
        /// if the card is not a french card
        /// </exception>
        /// <exception cref="WrongSuitException">
        /// if the new card's suit is not the AllowedSuit
        /// </exception>
        private void CheckSuit(object sender, ListChangedEventArgs e)
        {
            if (this.AllowedSuit == Suit.Unknown)
            {
                return;
            }

            this.TopCard = this.BindingListCards.Count > 0 ? (FrenchCard)this.BindingListCards[this.BindingListCards.Count - 1] : null;

            if (e.ListChangedType != ListChangedType.ItemAdded)
            {
                return;
            }

            var card = this.BindingListCards[e.NewIndex] as FrenchCard;
            if (card == null)
            {
                throw new ArgumentException("Card must be a FrenchCard!");
            }

            card.FaceUp = true;

            if (card.Suit != this.AllowedSuit)
            {
                throw new WrongSuitException(card.Suit, this.AllowedSuit);
            }
        }
    }
}