﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PileLayout.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    /// <summary>
    /// List of supported visual configurations of card collections. Used for visual depiction.
    /// </summary>
    public enum PileLayout
    {
        /// <summary>For holding exactly one card face up</summary>
        Cell,

        /// <summary>The cards are exactly on top of each other. Face is unspecified</summary>
        Squared,

        /// <summary>A squared pile where all cards must be face down. (eg. the deck)</summary>
        Stock,

        /// <summary>A squared pile where all cards are face-up. Default implementation also enforces single-suit pile.</summary>
        Foundation,

        /// <summary>A downward fanned pile with enoughspace so the card name is visible for each face-up card</summary>
        Tableau,
    }
}