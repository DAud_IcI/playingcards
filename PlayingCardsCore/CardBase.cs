﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardBase.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Abstract base class for ICard
    /// </summary>
    public abstract class CardBase : ICard
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardBase"/> class.
        /// </summary>
        public CardBase()
        {
            this.Data = new Dictionary<string, object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CardBase"/> class.
        /// </summary>
        /// <param name="name">name of the card</param>
        /// <param name="face_up">true if the card's face is visible</param>
        /// <param name="orientation">which direction the card's top edge is facing</param>
        /// <param name="aspect_ratio">width / height of the card (more flexible than hard-coded size)</param>
        /// <param name="data">extra data</param>
        /// <param name="action">event handler</param>
        public CardBase(
            string name,
            bool face_up,
            CardOrientation orientation,
            double aspect_ratio,
            IDictionary<string, object> data,
            EventHandler<CardActionEventArgs> action = null)
            : this()
        {
            this.Name = name;
            this.FaceUp = face_up;
            this.Orientation = orientation;
            this.AspectRatio = aspect_ratio;
            if (action != null)
            {
                this.Action += action;
            }

            if (this.Data != null)
            {
                this.Data = data;
            }
        }

        /// <summary>
        /// Action event
        /// </summary>
        public event EventHandler<CardActionEventArgs> Action;

        /// <summary>
        /// Gets the card's aspect ratio.
        /// It's width / height of the card (more flexible than hard-coded size).
        /// </summary>
        public double AspectRatio { get; private set; }

        /// <summary>
        /// Gets or sets the background image.
        /// </summary>
        public string BackImage { get; set; }

        /// <summary>
        /// Gets the extra data
        /// </summary>
        public IDictionary<string, object> Data { get; private set; }

        /// <summary>
        /// Gets or sets the face image.
        /// </summary>
        public string FaceImage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the card is facing up.
        /// </summary>
        public bool FaceUp { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the orientation.
        /// </summary>
        public CardOrientation Orientation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the name should be displayed.
        /// </summary>
        public bool ShowName { get; set; }

        /// <summary>
        /// Triggers the Action event.
        /// </summary>
        /// <param name="e">event data</param>
        public void OnAction(CardActionEventArgs e)
        {
            this.Action?.Invoke(this, e);
        }
    }
}