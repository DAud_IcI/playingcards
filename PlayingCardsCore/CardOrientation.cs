﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardOrientation.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    /// <summary>
    /// represents a direction the card's top may be facing
    /// </summary>
    public enum CardOrientation
    {
        /// <summary>
        /// The up.
        /// </summary>
        Up,

        /// <summary>
        /// The right.
        /// </summary>
        Right,

        /// <summary>
        /// The down.
        /// </summary>
        Down,

        /// <summary>
        /// The left.
        /// </summary>
        Left
    }
}