﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameBase.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Abstract base class for IGame
    /// </summary>
    public abstract class GameBase : IGame
    {
        /// <summary>
        /// The turn state.
        /// </summary>
        private string turnState = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameBase"/> class.
        /// </summary>
        public GameBase()
        {
            CurrentGame = this;

            this.Players = new BindingList<IPlayer>();
            this.Layout = new BindingList<IPile>();

            this.TurnStateChanged += this.TurnState_Initialize;
        }

        /// <summary>
        /// The game over event.
        /// </summary>
        public event EventHandler<GameOverEventArgs> GameOver;

        /// <summary>
        /// The turn state changed event.
        /// </summary>
        public event EventHandler<TurnStateEventArgs> TurnStateChanged;

        /// <summary>
        /// Gets or sets the current game.
        /// </summary>
        public static IGame CurrentGame { get; set; }

        /// <summary>
        /// Gets or sets the background image.
        /// </summary>
        public virtual string BackgroundImage { get; protected set; }

        /// <summary>
        /// Gets the game name.
        /// </summary>
        public virtual string GameName
        {
            get
            {
                return this.GetType().ToString();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the game is single player.
        /// </summary>
        public virtual bool IsSinglePlayer
        {
            get
            {
                return this.Players.Count == 1;
            }
        }

        /// <summary>
        /// Gets or sets the layout.
        /// </summary>
        public IList<IPile> Layout { get; set; }

        /// <summary>
        /// Gets the players.
        /// </summary>
        public IList<IPlayer> Players { get; private set; }

        /// <summary>
        /// Gets the turn count.
        /// </summary>
        public int TurnCount { get; private set; }

        /// <summary>
        /// Gets or sets the turn state.
        /// </summary>
        public virtual string TurnState
        {
            get
            {
                return this.turnState;
            }

            set
            {
                if (this.turnState == value)
                {
                    return;
                }

                var args = new TurnStateEventArgs(this.turnState, value);

                this.turnState = value;
                this.OnTurnStateChanged(args);
            }
        }

        /// <summary>
        /// Deserialises the class
        /// </summary>
        /// <param name="path">source file</param>
        /// <returns>the loaded-in IGame instance</returns>
        public abstract IGame Deserialize(string path);

        /// <summary>
        /// Finds a pile based on its name
        /// </summary>
        /// <param name="name">pile name</param>
        /// <returns>found pile or null</returns>
        public IPile GetPile(string name)
        {
            foreach (IPile pile in this.Layout)
            {
                if (pile.Name == name)
                {
                    return pile;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds a pile based on a card it contains
        /// </summary>
        /// <param name="card">search card</param>
        /// <returns>found pile or null</returns>
        public IPile GetPile(ICard card)
        {
            foreach (IPile pile in this.Layout)
            {
                if (pile.Cards.Contains(card))
                {
                    return pile;
                }
            }

            return null;
        }

        /// <summary>
        /// Abstract initialize.
        /// </summary>
        public abstract void Initialize();

        /// <summary>
        /// Vitrtual initialize turn.
        /// </summary>
        public virtual void InitializeTurn()
        {
        }

        /// <summary>
        /// End game with the player with the highest score as winner.
        /// </summary>
        public virtual void OnGameOver()
        {
            this.OnGameOver(null);
        }

        /// <summary>
        /// End game with the specified player as winner.
        /// </summary>
        /// <param name="winner">the player who have won the game.</param>
        public virtual void OnGameOver(IPlayer winner)
        {
            if (this.GameOver != null)
            {
                if (winner == null)
                {
                    winner = this.Players.OrderByDescending(x => x.Score).First();
                }

                this.GameOver(this, new GameOverEventArgs(winner));
            }
        }

        /// <summary>
        /// The on turn state changed.
        /// </summary>
        /// <param name="args">
        /// event args.
        /// </param>
        public virtual void OnTurnStateChanged(TurnStateEventArgs args)
        {
            if (this.TurnStateChanged != null)
            {
                this.TurnStateChanged(this, args);
            }
        }

        /// <summary>
        /// Serialises the class
        /// </summary>
        /// <param name="path">destination file</param>
        public abstract void Serialize(string path);

        /// <summary>
        /// Initialise turn
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void TurnState_Initialize(object sender, TurnStateEventArgs e)
        {
            if (e.NewState == TurnStateEventArgs.TurnStateInit)
            {
                this.TurnCount++;
                this.InitializeTurn();
                this.TurnState = TurnStateEventArgs.TurnStateReady;
            }
        }
    }
}