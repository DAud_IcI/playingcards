﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Pile.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// The pile base implementation.
    /// </summary>
    [DataContract]
    public class Pile : IPile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pile"/> class.
        /// </summary>
        public Pile()
        {
            this.BindingListCards = new BindingList<ICard>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pile"/> class.
        /// </summary>
        /// <param name="layout">
        /// The layout.
        /// </param>
        public Pile(PileLayout layout)
            : this()
        {
            this.Layout = layout;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pile"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="top">
        /// The row index.
        /// </param>
        /// <param name="left">
        /// The column index.
        /// </param>
        /// <param name="layout">
        /// The layout.
        /// </param>
        public Pile(string name, int top, int left, PileLayout layout)
            : this(layout)
        {
            this.Name = name;
            this.Top = top;
            this.Left = left;
        }

        /// <summary>
        /// Gets the cards.
        /// </summary>
        [DataMember]
        public IList<ICard> Cards
        {
            get
            {
                return this.BindingListCards;
            }
        }

        /// <summary>
        /// Gets or sets the layout.
        /// </summary>
        [DataMember]
        public PileLayout Layout { get; set; }

        /// <summary>
        /// Gets or sets the column index.
        /// </summary>
        [DataMember]
        public int Left { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the row index.
        /// </summary>
        [DataMember]
        public int Top { get; set; }

        /// <summary>
        /// Gets the cards as BindingList.
        /// </summary>
        protected BindingList<ICard> BindingListCards { get; private set; }
    }
}