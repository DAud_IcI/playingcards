﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICard.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The Card interface.
    /// </summary>
    public interface ICard
    {
        /// <summary>
        /// The action event.
        /// </summary>
        event EventHandler<CardActionEventArgs> Action;

        /// <summary>
        /// Gets the aspect ratio.
        /// </summary>
        double AspectRatio { get; }

        /// <summary>
        /// Gets or sets the background image.
        /// </summary>
        string BackImage { get; set; }

        /// <summary>
        /// Gets the extra data.
        /// </summary>
        IDictionary<string, object> Data { get; }

        /// <summary>
        /// Gets or sets the face image.
        /// </summary>
        string FaceImage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether face should be up.
        /// </summary>
        bool FaceUp { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the card orientation.
        /// </summary>
        CardOrientation Orientation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether name should be shown.
        /// </summary>
        bool ShowName { get; set; }

        /// <summary>
        /// Triggers the Action event.
        /// </summary>
        /// <param name="e">event data</param>
        void OnAction(CardActionEventArgs e);
    }
}