﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="INotifyPropertyChangedExtension.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace System.ComponentModel
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// reusable binding notification extension methods
    /// </summary>
    public static class INotifyPropertyChangedExtension
    {
        /// <summary>
        /// Triggers PropertyChanged
        /// </summary>
        /// <param name="handler">event handler</param>
        /// <param name="sender">sender object</param>
        /// <param name="name">name of caller method</param>
        public static void Notify(
            this PropertyChangedEventHandler handler,
            INotifyPropertyChanged sender,
            [CallerMemberName] string name = "")
        {
            handler?.Invoke(sender, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Updates the value and triggers PropertyChanged.
        /// </summary>
        /// <typeparam name="T">type of the value.</typeparam>
        /// <param name="handler">event handler</param>
        /// <param name="sender">sender object</param>
        /// <param name="target">variable to be updated</param>
        /// <param name="value">new value</param>
        /// <param name="name">name of caller method</param>
        public static void Update<T>(
            this PropertyChangedEventHandler handler,
            INotifyPropertyChanged sender,
            ref T target,
            T value,
            [CallerMemberName] string name = "")
        {
            target = value;
            handler?.Invoke(sender, new PropertyChangedEventArgs(name));
        }
    }
}