﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGame.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The Game interface.
    /// </summary>
    public interface IGame
    {
        /// <summary>
        /// The game over event.
        /// </summary>
        event EventHandler<GameOverEventArgs> GameOver;

        /// <summary>
        /// The turn state changed event.
        /// </summary>
        event EventHandler<TurnStateEventArgs> TurnStateChanged;

        /// <summary>
        /// Gets the background image.
        /// </summary>
        string BackgroundImage { get; }

        /// <summary>
        /// Gets the game name.
        /// </summary>
        string GameName { get; }

        /// <summary>
        /// Gets a value indicating whether is single player.
        /// </summary>
        bool IsSinglePlayer { get; }

        /// <summary>
        /// Gets the layout.
        /// </summary>
        IList<IPile> Layout { get; }

        /// <summary>
        /// Gets the players.
        /// </summary>
        IList<IPlayer> Players { get; }

        /// <summary>
        /// Gets the turn count.
        /// </summary>
        int TurnCount { get; }

        /// <summary>
        /// Gets or sets the turn state.
        /// </summary>
        string TurnState { get; set; }
        
        /// <summary>
        /// Deserialises the class
        /// </summary>
        /// <param name="path">source file</param>
        /// <returns>the loaded-in IGame instance</returns>
        IGame Deserialize(string path);

        /// <summary>
        /// Finds a pile based on its name
        /// </summary>
        /// <param name="name">pile name</param>
        /// <returns>found pile or null</returns>
        IPile GetPile(string name);

        /// <summary>
        /// Finds a pile based on a card it contains
        /// </summary>
        /// <param name="card">search card</param>
        /// <returns>found pile or null</returns>
        IPile GetPile(ICard card);

        /// <summary>
        /// Initializes the game
        /// </summary>
        void Initialize();

        /// <summary>
        /// Initializes the turn
        /// </summary>
        void InitializeTurn();

        /// <summary>
        /// Serialises the class
        /// </summary>
        /// <param name="path">destination file</param>
        void Serialize(string path);

        /// <summary>
        /// End game with the player with the highest score as winner.
        /// </summary>
        void OnGameOver();
    }
}