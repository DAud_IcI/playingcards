﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOverEventArgs.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    using System;

    /// <summary>
    /// The game over event args.
    /// </summary>
    public class GameOverEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameOverEventArgs"/> class.
        /// </summary>
        /// <param name="winner">
        /// The winner.
        /// </param>
        public GameOverEventArgs(IPlayer winner)
        {
            this.Winner = winner;
        }

        /// <summary>
        /// Gets or sets the winner.
        /// </summary>
        public IPlayer Winner { get; set; }
    }
}