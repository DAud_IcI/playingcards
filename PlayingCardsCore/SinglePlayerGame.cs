﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SinglePlayerGame.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System.ComponentModel;

    /// <summary>
    /// Base class for singleplayer games.
    /// </summary>
    public abstract class SinglePlayerGame : GameBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SinglePlayerGame"/> class.
        /// </summary>
        protected SinglePlayerGame()
        {
            this.Layout = new BindingList<IPile>();
            this.Players.Add(new Player("Player"));
        }

        /// <summary>
        /// Gets a value indicating whether is single player. The answer is always true.
        /// </summary>
        public override bool IsSinglePlayer
        {
            get
            {
                return true;
            }
        }
    }
}