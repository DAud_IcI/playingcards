﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TurnStateEventArgs.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The turn state event args.
    /// </summary>
    public class TurnStateEventArgs : EventArgs
    {
        /// <summary>
        /// The turn state: game over.
        /// </summary>
        public const string TurnStateGameOver = "game over";

        /// <summary>
        /// The turn state: init.
        /// </summary>
        public const string TurnStateInit = "init";

        /// <summary>
        /// The turn state: over.
        /// </summary>
        public const string TurnStateOver = "over";

        /// <summary>
        /// The turn state: ready.
        /// </summary>
        public const string TurnStateReady = "ready";

        /// <summary>
        /// Initializes a new instance of the <see cref="TurnStateEventArgs"/> class.
        /// </summary>
        public TurnStateEventArgs()
        {
            this.Data = new Dictionary<string, object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TurnStateEventArgs"/> class.
        /// </summary>
        /// <param name="previous_state">
        /// The previous state.
        /// </param>
        /// <param name="new_state">
        /// The new state.
        /// </param>
        public TurnStateEventArgs(string previous_state, string new_state)
            : this()
        {
            this.PreviousState = previous_state;
            this.NewState = new_state;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TurnStateEventArgs"/> class.
        /// </summary>
        /// <param name="previous_state">
        /// The previous state.
        /// </param>
        /// <param name="new_state">
        /// The new state.
        /// </param>
        /// <param name="data">
        /// The extra data.
        /// </param>
        public TurnStateEventArgs(string previous_state, string new_state, Dictionary<string, object> data)
            : this(previous_state, new_state)
        {
            this.Data = data;
        }

        /// <summary>
        /// Gets or sets the extra data.
        /// </summary>
        public Dictionary<string, object> Data { get; set; }

        /// <summary>
        /// Gets or sets the new state.
        /// </summary>
        public string NewState { get; set; }

        /// <summary>
        /// Gets or sets the previous state.
        /// </summary>
        public string PreviousState { get; set; }
    }
}