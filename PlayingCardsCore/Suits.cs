﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Suits.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The suits static helper class.
    /// </summary>
    public static class Suits
    {
        /// <summary>
        /// Iteration helper.
        /// </summary>
        private static readonly int[] Range4 = { 0, 1, 2, 3 };

        /// <summary>
        /// Initializes static members of the <see cref="Suits"/> class.
        /// </summary>
        static Suits()
        {
            Joker = "JOKER";
            Numbers = new[] { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
            Symbols = "♠♣♥♦".ToCharArray();
        }

        /// <summary>
        /// Gets the Suit => char dictionary.
        /// </summary>
        public static IReadOnlyDictionary<Suit, char> Chars
        {
            get
            {
                return Range4.ToDictionary(index => (Suit)(index + 1), index => Symbols[index]);
            }
        }

        /// <summary>
        /// Gets the clubs.
        /// </summary>
        public static char Clubs
        {
            get
            {
                return Symbols[1];
            }
        }

        /// <summary>
        /// Gets the diamonds.
        /// </summary>
        public static char Diamonds
        {
            get
            {
                return Symbols[3];
            }
        }

        /// <summary>
        /// Gets the char => Suits dictionary.
        /// </summary>
        public static IReadOnlyDictionary<char, Suit> Enums
        {
            get
            {
                return Range4.ToDictionary(index => Symbols[index], index => (Suit)(index + 1));
            }
        }

        /// <summary>
        /// Gets the hearts.
        /// </summary>
        public static char Hearts
        {
            get
            {
                return Symbols[2];
            }
        }

        /// <summary>
        /// Gets or sets the joker.
        /// </summary>
        public static string Joker { get; set; }

        /// <summary>
        /// Gets the max number.
        /// </summary>
        public static int MaxNumber
        {
            get
            {
                return Numbers.Length;
            }
        }

        /// <summary>
        /// Gets or sets the numbers.
        /// </summary>
        public static string[] Numbers { get; set; }

        /// <summary>
        /// Gets the spades.
        /// </summary>
        public static char Spades
        {
            get
            {
                return Symbols[0];
            }
        }

        /// <summary>
        /// Gets or sets the symbols.
        /// </summary>
        public static char[] Symbols { get; set; }

        /// <summary>
        /// Get the colour of a suit.
        /// </summary>
        /// <param name="suit">
        /// The suit.
        /// </param>
        /// <returns>
        /// The <see cref="Colour"/> of the given suit.
        /// </returns>
        public static Colour GetColour(Suit suit)
        {
            if (suit == Suit.Spades || suit == Suit.Clubs)
            {
                return Colour.Black;
            }

            return Colour.Red;
        }

        /// <summary>
        /// Gets if the character represents a suit.
        /// </summary>
        /// <param name="symbol">
        /// The symbol.
        /// </param>
        /// <returns>
        /// True if the character corresponds to a suit symbol
        /// </returns>
        public static bool IsSuit(char symbol)
        {
            return ((IList<char>)Symbols).Contains(symbol);
        }
    }
}