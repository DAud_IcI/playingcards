﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrenchCard.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a card from French playing cards.
    /// </summary>
    public class FrenchCard : ICard
    {
        /// <summary>
        /// The card name separator.
        /// </summary>
        private static string cardNameSeparator = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrenchCard"/> class.
        /// </summary>
        public FrenchCard()
        {
            this.IsJoker = false;
            this.AspectRatio = 0.75;
            this.ShowName = true;
            this.FaceImage = "!FrenchCard";
            this.BackImage = "#EE0022";
            this.Data = new Dictionary<string, object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrenchCard"/> class.
        /// </summary>
        /// <param name="suit">Either one of ♠, ♣, ♥ or ♦.</param>
        /// <param name="number">1 for Ace, 2-10 for the numbered cards, 11-13 for the face cards (Jack, Queen a King)</param>
        /// <param name="face_up">True if the front of the card is facing up.</param>
        /// <param name="orientation">Which direction the top of the card facing.</param>
        public FrenchCard(Suit suit, int number, bool face_up = false, CardOrientation orientation = CardOrientation.Up)
            : this()
        {
            if (suit == Suit.Unknown)
            {
                throw new ArgumentException("The card suit must not be initialized as 'Unknon'!");
            }

            this.Suit = suit;
            this.Number = number;
            this.FaceUp = face_up;
            this.Orientation = orientation;
        }

        /// <summary>
        /// Card event Action
        /// </summary>
        public event EventHandler<CardActionEventArgs> Action;

        /// <summary>
        /// Gets the aspect ratio.
        /// </summary>
        public double AspectRatio { get; private set; }

        /// <summary>
        /// Gets or sets the background image.
        /// </summary>
        public string BackImage { get; set; }

        /// <summary>
        /// Gets the data.
        /// </summary>
        public IDictionary<string, object> Data { get; private set; }

        /// <summary>
        /// Gets or sets the face image.
        /// </summary>
        public string FaceImage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the card is facing up.
        /// </summary>
        public bool FaceUp { get; set; }

        /// <summary>
        /// Gets a value indicating whether is a joker.
        /// </summary>
        public bool IsJoker { get; private set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <exception cref="InvalidOperationException">Can't change card name!</exception>
        public string Name
        {
            get
            {
                return this.IsJoker
                           ? Suits.Joker
                           : string.Format(
                               "{0}{1}{2}",
                               Suits.Chars[this.Suit],
                               cardNameSeparator,
                               Suits.Numbers[this.Number - 1]);
            }

            set
            {
                throw new InvalidOperationException("Can't change card name!");
            }
        }

        /// <summary>
        /// Gets the number of the card.
        /// </summary>
        public int Number { get; private set; }

        /// <summary>
        /// Gets or sets the orientation.
        /// </summary>
        public CardOrientation Orientation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show name.
        /// </summary>
        public bool ShowName { get; set; }

        /// <summary>
        /// Gets the suit.
        /// </summary>
        public Suit Suit { get; private set; }

        /// <summary>
        /// Creates a Joker card
        /// </summary>
        /// <param name="face_up">True if the front of the card is facing up.</param>
        /// <param name="orientation">Which direction the top of the card facing.</param>
        /// <returns>Joker card</returns>
        public static FrenchCard CreateJoker(bool face_up = false, CardOrientation orientation = CardOrientation.Up)
        {
            return new FrenchCard(Suit.Spades, 0, face_up, orientation) { IsJoker = true, Suit = Suit.Unknown };
        }

        /// <summary>
        /// Triggers the Action event.
        /// </summary>
        /// <param name="e">event data</param>
        public void OnAction(CardActionEventArgs e)
        {
            this.Action?.Invoke(this, e);
        }
    }
}