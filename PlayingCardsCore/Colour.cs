﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Colour.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    /// <summary>
    /// The colour of a suit.
    /// </summary>
    public enum Colour
    {
        /// <summary>
        /// The black.
        /// </summary>
        Black,

        /// <summary>
        /// The red.
        /// </summary>
        Red
    }
}