﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPlayer.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System;

    /// <summary>
    /// The Player interface.
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the amount of the colour blue.
        /// </summary>
        byte PlayerColourBlue { get; set; }

        /// <summary>
        /// Gets or sets the amount of the colour green.
        /// </summary>
        byte PlayerColourGreen { get; set; }

        /// <summary>
        /// Gets or sets the amount of the colour red.
        /// </summary>
        byte PlayerColourRed { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        int Score { get; set; }
    }

    /// <summary>
    /// The player class.
    /// </summary>
    public class Player : IPlayer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
            : this(string.Empty, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="score">
        /// The score.
        /// </param>
        public Player(string name, int score = 0)
        {
            this.Name = name;
            this.Score = score;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="colour">
        /// The colour in "RRGGBB" hex format.
        /// </param>
        /// <param name="score">
        /// The score.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Colour string must be in the RRGGBB format with hexadecimal numbers.
        /// </exception>
        public Player(string name, string colour, int score = 0)
            : this(name, score)
        {
            if (colour.Length != 6)
            {
                throw new ArgumentException("Colour string must be in the RRGGBB format with hexadecimal numbers.");
            }

            this.PlayerColourRed = Convert.ToByte(colour.Substring(0, 2), 16);
            this.PlayerColourGreen = Convert.ToByte(colour.Substring(2, 2), 16);
            this.PlayerColourBlue = Convert.ToByte(colour.Substring(4, 2), 16);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="red">
        /// The red.
        /// </param>
        /// <param name="green">
        /// The green.
        /// </param>
        /// <param name="blue">
        /// The blue.
        /// </param>
        /// <param name="score">
        /// The score.
        /// </param>
        public Player(string name, byte red, byte green, byte blue, int score = 0)
            : this(name, score)
        {
            this.PlayerColourRed = red;
            this.PlayerColourGreen = green;
            this.PlayerColourBlue = blue;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the amount of the colour blue.
        /// </summary>
        public byte PlayerColourBlue { get; set; }

        /// <summary>
        /// Gets or sets the amount of the colour green.
        /// </summary>
        public byte PlayerColourGreen { get; set; }

        /// <summary>
        /// Gets or sets the amount of the colour red.
        /// </summary>
        public byte PlayerColourRed { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public int Score { get; set; }
    }
}