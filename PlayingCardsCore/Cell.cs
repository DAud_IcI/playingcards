﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Cell.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    using System.ComponentModel;

    /// <summary>
    /// The cell. For holding exactly one card face up.
    /// </summary>
    public class Cell : Pile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cell"/> class.
        /// </summary>
        public Cell()
            : base(PileLayout.Cell)
        {
            this.BindingListCards.ListChanged += this.Overwrite;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cell"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="top">
        /// The top.
        /// </param>
        /// <param name="left">
        /// The left.
        /// </param>
        public Cell(string name, int top, int left)
            : base(name, top, left, PileLayout.Cell)
        {
            this.BindingListCards.ListChanged += this.Overwrite;
        }

        /// <summary>
        /// Gets or sets the card.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public ICard Card
        {
            get
            {
                return this.BindingListCards.Count > 0 ? this.BindingListCards[0] : null;
            }

            set
            {
                if (this.BindingListCards.Count == 0)
                {
                    this.BindingListCards.Add(value);
                }
                else
                {
                    this.BindingListCards[0] = value;
                }
            }
        }

        /// <summary>
        /// overwrite card.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void Overwrite(object sender, ListChangedEventArgs e)
        {
            if (this.BindingListCards.Count <= 1)
            {
                return;
            }

            ICard card = this.BindingListCards[e.NewIndex];
            this.BindingListCards.Clear();
            this.Card = card;
        }
    }
}