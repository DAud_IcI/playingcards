﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Suit.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    /// <summary>
    /// The suit of French cards.
    /// </summary>
    public enum Suit
    {
        /// <summary>
        /// unknown suit.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The spades.
        /// </summary>
        Spades = 1,

        /// <summary>
        /// The clubs.
        /// </summary>
        Clubs = 2,

        /// <summary>
        /// The hearts.
        /// </summary>
        Hearts = 3,

        /// <summary>
        /// The diamons.
        /// </summary>
        Diamons = 4
    }
}