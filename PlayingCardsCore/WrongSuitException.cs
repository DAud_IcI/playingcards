﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WrongSuitException.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PlayingCards.Core
{
    using System;

    /// <summary>
    /// The wrong suit exception.
    /// </summary>
    public class WrongSuitException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WrongSuitException"/> class.
        /// </summary>
        /// <param name="given">
        /// The suit given.
        /// </param>
        /// <param name="expected">
        /// The suit expected.
        /// </param>
        public WrongSuitException(Suit given, Suit expected)
            : base(string.Format("Suit {0} was given instead of the expected {1}", given, expected))
        {
            this.Given = given;
            this.Expected = expected;
        }

        /// <summary>
        /// Gets or sets the suit expected.
        /// </summary>
        public Suit Expected { get; set; }

        /// <summary>
        /// Gets or sets the suit given.
        /// </summary>
        public Suit Given { get; set; }
    }
}