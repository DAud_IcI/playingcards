﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardActionException.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System;

    /// <summary>
    /// Exception relating to Card.Action
    /// </summary>
    public class CardActionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardActionException"/> class.
        /// </summary>
        /// <param name="message">Error message</param>
        /// <param name="sender">card to trigger the exception</param>
        /// <param name="e">card action event arguments</param>
        public CardActionException(string message, object sender, CardActionEventArgs e)
            : base(message)
        {
            this.Sender = sender;
            this.EventArgs = e;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CardActionException"/> class.
        /// </summary>
        /// <param name="inner">inner exception (provides message)</param>
        /// <param name="sender">card to trigger the exception</param>
        /// <param name="e">card action event arguments</param>
        public CardActionException(Exception inner, object sender, CardActionEventArgs e)
            : base(inner.Message, inner)
        {
            this.Sender = sender;
            this.EventArgs = e;
        }

        /// <summary>
        /// Gets or sets the event args.
        /// </summary>
        public CardActionEventArgs EventArgs { get; set; }

        /// <summary>
        /// Gets or sets the sender.
        /// </summary>
        public object Sender { get; set; }
    }
}