﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardActionEventArgs.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Event data for a Card.Action
    /// </summary>
    public class CardActionEventArgs : EventArgs
    {
        /// <summary>
        /// Action type: drag on empty pile.
        /// </summary>
        public const string ActionDragOnEmptyPile = "drag on empty pile";

        /// <summary>
        /// Action type: drag on other card.
        /// </summary>
        public const string ActionDragOnOtherCard = "drag on other card";

        /// <summary>
        /// Data header: pile from.
        /// </summary>
        public const string DataPileFrom = "pile from";

        /// <summary>
        /// Data header: pile to.
        /// </summary>
        public const string DataPileTo = "pile to";

        /// <summary>
        /// Initializes a new instance of the <see cref="CardActionEventArgs"/> class.
        /// </summary>
        public CardActionEventArgs()
        {
            this.Data = new Dictionary<string, object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CardActionEventArgs"/> class.
        /// </summary>
        /// <param name="target">the card being interacted with</param>
        /// <param name="action_name">the name of the interaction</param>
        public CardActionEventArgs(ICard target, string action_name)
            : this()
        {
            this.Target = target;
            this.ActionName = action_name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CardActionEventArgs"/> class.
        /// </summary>
        /// <param name="target">the card being interacted with</param>
        /// <param name="action_name">the name of the interaction</param>
        /// <param name="data">additional data</param>
        public CardActionEventArgs(ICard target, string action_name, Dictionary<string, object> data)
            : this(target, action_name)
        {
            if (data != null)
            {
                this.Data = data;
            }
        }

        /// <summary>
        /// Gets or sets the action name.
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public Dictionary<string, object> Data { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        public ICard Target { get; set; }
    }
}