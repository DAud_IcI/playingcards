﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPile.cs" company="OE-NIK">
//   No copyright (public domain)
// </copyright>
// <summary>
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace PlayingCards.Core
{
    using System.Collections.Generic;

    /// <summary>
    /// The Pile interface.
    /// </summary>
    public interface IPile
    {
        /// <summary>
        /// Gets the cards.
        /// </summary>
        IList<ICard> Cards { get; }

        /// <summary>
        /// Gets the layout.
        /// </summary>
        PileLayout Layout { get; }

        /// <summary>
        /// Gets or sets the column index.
        /// </summary>
        int Left { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the row index.
        /// </summary>
        int Top { get; set; }
    }
}