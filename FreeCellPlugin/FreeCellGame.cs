﻿namespace FreeCellPlugin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    using PlayingCards.Core;

    /// <summary>
    /// Implementation of an IGame that contains the structure and logic for the FreeCell solitaire game.
    /// </summary>
    public class FreeCellGame : SinglePlayerGame
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FreeCellGame" /> class.
        /// Call Initialize to get it ready!
        /// </summary>
        public FreeCellGame()
        {
            this.FreeCells = new BindingList<Cell>();
            this.Foundations = new BindingList<FoundationFrench>();
            this.Tableaus = new BindingList<IPile>();

            this.BackgroundImage = "Images/greenfelt.png";
        }

        /// <summary>
        /// Gets or sets a collection that contains all generated cards in the current game.
        /// </summary>
        public BindingList<FrenchCard> AllCards { get; set; }

        /// <summary>Gets or sets foundation for the Clubs suit.</summary>
        public FoundationFrench Clubs { get; set; }

        /// <summary>Gets or sets foundation for the Diamons suit.</summary>
        public FoundationFrench Diamons { get; set; }

        /// <summary>
        /// Gets or sets a collection of all 4 suit foundations
        /// </summary>
        public BindingList<FoundationFrench> Foundations { get; set; }

        /// <summary>
        /// Gets or sets a collection of Cell type single-card piles
        /// </summary>
        public BindingList<Cell> FreeCells { get; set; }

        /// <summary>
        /// gets "FreeCell" 
        /// </summary>
        public override string GameName
        {
            get
            {
                return "FreeCell";
            }
        }

        /// <summary>Gets or sets foundation for the Hearts suit.</summary>
        public FoundationFrench Hearts { get; set; }

        /// <summary>Gets or sets foundation for the Spades suit.</summary>
        public FoundationFrench Spades { get; set; }

        /// <summary>
        /// Gets or sets A collection of all tableau piles that initially contains the shuffled cards
        /// </summary>
        public BindingList<IPile> Tableaus { get; set; }

        /// <summary>
        /// TODO: currently serialisation is planned, but not supported
        /// </summary>
        /// <param name="path">The save file's path</param>
        /// <returns>Deserialised instance of this class.</returns>
        public override IGame Deserialize(string path)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets up the game so it's ready to be played.
        /// </summary>
        public override void Initialize()
        {
            this.Players.First().Score = 100;
            this.SetupLayout();
            this.SetupShuffle();
            this.SetupEvents();
        }

        /// <summary>
        /// TODO: currently serialisation is planned, but not supported
        /// </summary>
        /// <param name="path">The save file's path</param>
        public override void Serialize(string path)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Attach the card and game events.
        /// </summary>
        internal void SetupEvents()
        {
            foreach (var card in this.AllCards)
            {
                card.Action += this.Card_Drag;
            }

            this.TurnStateChanged += this.TurnState_Over;

            this.TurnState = TurnStateEventArgs.TurnStateInit;
        }

        /// <summary>
        /// Create all the empty piles on the table:
        /// 4 free cells,
        /// 4 foundations for each suit and
        /// 8 tableaus.
        /// </summary>
        internal void SetupLayout()
        {
            for (int i = 0; i < 4; i++)
            {
                var cell = new Cell(string.Format("free cell {0}", i + 1), 0, i);
                this.FreeCells.Add(cell);
                this.Layout.Add(cell);
            }

            this.Layout.Add(this.Spades = new FoundationFrench(Suit.Spades, 0, 4));
            this.Layout.Add(this.Clubs = new FoundationFrench(Suit.Clubs, 0, 5));
            this.Layout.Add(this.Hearts = new FoundationFrench(Suit.Hearts, 0, 6));
            this.Layout.Add(this.Diamons = new FoundationFrench(Suit.Diamons, 0, 7));

            this.Foundations.Add(this.Spades);
            this.Foundations.Add(this.Clubs);
            this.Foundations.Add(this.Hearts);
            this.Foundations.Add(this.Diamons);

            for (int i = 0; i < 8; i++)
            {
                var tableau = new Pile(string.Format("tableau {0}", i + 1), 1, i, PileLayout.Tableau);
                this.Tableaus.Add(tableau);
                this.Layout.Add(tableau);
            }
        }

        /// <summary>
        /// Create and save one full deck of French cards (without jokers).
        /// Then shuffle the deck and evenly distribute the cards on the 8 tableaus.
        /// </summary>
        internal void SetupShuffle()
        {
            var random = new Random();

            // create a full sorted deck
            var cards = new List<FrenchCard>(13 * 4);
            this.AllCards = new BindingList<FrenchCard>();
            foreach (Suit suit in Suits.Enums.Values)
            {
                for (int i = 1; i <= 13; i++)
                {
                    var card = new FrenchCard(suit, i, true);
                    cards.Add(card);
                    this.AllCards.Add(card);
                }
            }

            // pick a random card and place it onto the next tableau until the temp. deck is empty
            int index = 0;
            while (cards.Count > 0)
            {
                int from = random.Next(cards.Count);
                this.Tableaus[index].Cards.Add(cards[from]);
                cards.RemoveAt(from);
                index = (index + 1) % this.Tableaus.Count;
            }
        }

        /// <summary>
        /// Handles a card being dragged on top another card or an empty pile.
        /// </summary>
        /// <param name="sender">The FrenchCard that triggered the event</param>
        /// <param name="e">Event data.</param>
        private void Card_Drag(object sender, CardActionEventArgs e)
        {
            if (e.ActionName != CardActionEventArgs.ActionDragOnOtherCard
                && e.ActionName != CardActionEventArgs.ActionDragOnEmptyPile)
            {
                return;
            }

            FrenchCard current = (FrenchCard)sender;

            IPile from = e.Data.ContainsKey(CardActionEventArgs.DataPileFrom)
                             ? (IPile)e.Data[CardActionEventArgs.DataPileFrom]
                             : this.GetPile(current);
            IPile to = e.Data.ContainsKey(CardActionEventArgs.DataPileTo)
                           ? (IPile)e.Data[CardActionEventArgs.DataPileTo]
                           : this.GetPile((FrenchCard)e.Target);
            if (from == to)
            {
                return;
            }
            
            int free_cell_count = 0;
            int from_index = from.Cards.IndexOf(current);
            FrenchCard next = null;
            foreach (var cell in this.FreeCells)
            {
                if (cell.Card == null)
                {
                    free_cell_count++;
                }
            }

            if (from_index < from.Cards.Count - 1)
            {
                if (from.Cards.Count > from_index + 1 + free_cell_count)
                {
                    throw new CardActionException("There are not enough free cells to pick up this card!", sender, e);
                }

                for (int i = from_index + 1; i < from.Cards.Count; i++)
                {
                    var prev = (FrenchCard)from.Cards[i - 1];
                    var curr = (FrenchCard)from.Cards[i];

                    if (prev.Number != curr.Number + 1)
                    {
                        throw new CardActionException("The cards above are not in sequence.", sender, e);
                    }

                    if (Suits.GetColour(prev.Suit) == Suits.GetColour(curr.Suit))
                    {
                        throw new CardActionException("The card colours must be alternating in the dragged sequence!", sender, e);
                    }
                }

                next = (FrenchCard)from.Cards[from_index + 1];
            }

            if (e.ActionName == CardActionEventArgs.ActionDragOnOtherCard)
            {
                // FrenchCard target = (FrenchCard)e.Target;
                FrenchCard target = (FrenchCard)to.Cards.Last();

                if (this.FreeCells.Contains(to))
                {
                    if ((to as Cell).Card != null)
                    {
                        throw new CardActionException("You can only place on a free cell!", sender, e);
                    }
                }
                else if (this.Foundations.Contains(to))
                {
                    if (current.Suit != target.Suit)
                    {
                        throw new CardActionException(new WrongSuitException(current.Suit, target.Suit), sender, e);
                    }

                    if (target.Number != current.Number - 1)
                    {
                        throw new CardActionException(
                                  string.Format(
                                      "The card must be a '{0}', but '{1}' was given.",
                                      Suits.Numbers[target.Number],
                                      Suits.Numbers[current.Number - 1]),
                                  sender,
                                  e);
                    }
                }
                else
                {
                    // if moving to tableau
                    // check number
                    if (current.Number == Suits.MaxNumber)
                    {
                        throw new CardActionException(
                                  "A King card can not be placed on any other one in the tableau!",
                                  sender,
                                  e);
                    }

                    if (target.Number != current.Number + 1)
                    {
                        throw new CardActionException(
                                  string.Format(
                                      "You can only place a '{0}'on top a '{1}', but your card is a '{2}'.",
                                      Suits.Numbers[target.Number - 2],
                                      Suits.Numbers[target.Number - 1],
                                      Suits.Numbers[current.Number - 1]),
                                  sender,
                                  e);
                    }

                    // check colour
                    Colour current_colour = Suits.GetColour(current.Suit);
                    if (current_colour == Suits.GetColour(target.Suit))
                    {
                        throw new CardActionException(
                                  string.Format(
                                      "You can only place a {0} card on top of a {1}.",
                                      current_colour == Colour.Black ? "black" : "red",
                                      current_colour == Colour.Black ? "red" : "black"),
                                  sender,
                                  e);
                    }
                }

                from.Cards.Remove(current);
                to.Cards.Add(current);
            }
            else if (e.ActionName == CardActionEventArgs.ActionDragOnEmptyPile)
            {
                if (to.Cards.Count > 1 || (to.Cards.Count == 1 && to.Cards[0] != null))
                {
                    e.ActionName = CardActionEventArgs.ActionDragOnOtherCard;
                    e.Target = to.Cards.Last();
                    this.Card_Drag(sender, e);
                    return;
                }

                if (this.Foundations.Contains(to))
                {
                    var foundation = to as FoundationFrench;
                    if (foundation.AllowedSuit != current.Suit)
                    {
                        throw new CardActionException(
                                  new WrongSuitException(current.Suit, foundation.AllowedSuit),
                                  sender,
                                  e);
                    }

                    if (current.Number != 1)
                    {
                        throw new CardActionException("The first card on the foundation must be an ace!", sender, e);
                    }
                }

                from.Cards.Remove(current);
                to.Cards.Add(current);
            }

            if (next != null)
            {
                this.Card_Drag(next, new CardActionEventArgs(current, CardActionEventArgs.ActionDragOnOtherCard));
            }
            else
            {
                // turn successfully completed after 1 card move
                this.TurnState = TurnStateEventArgs.TurnStateOver;
            }
        }

        /// <summary>
        /// Handles  turn end, transitions either to init or game over turn states.
        /// </summary>
        /// <param name="sender">This game.</param>
        /// <param name="e">Event data that contains the previous and new turn states</param>
        private void TurnState_Over(object sender, TurnStateEventArgs e)
        {
            if (e.NewState != TurnStateEventArgs.TurnStateOver)
            {
                return;
            }

            foreach (var foundation in this.Foundations)
            {
                if (foundation.TopCard == null || foundation.TopCard.Number != Suits.MaxNumber)
                {
                    this.Players.First().Score--;
                    this.TurnState = TurnStateEventArgs.TurnStateInit;
                    return;
                }
            }

            this.TurnState = TurnStateEventArgs.TurnStateGameOver;
            this.OnGameOver(this.Players[0]);
        }
    }
}