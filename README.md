# PlayingCards #
This is a framework for coding and running 2D card games.
Currently the focus is on solitaire style card games.

(the green play area is one TreeView containing the game entities - they form a DOM-like hierarchical structure, themed using the Game and Graphics plugins as well as extensive use of DataTemplates)
![screenshot](screenshot.png)

### What is in this repository? ###

It contains the main and development (dev_a2) branches of PlayingCards and the default included FreeCell plug-in.
The current versions on the branches are alpha 0.1.0 and 0.1.2 respectively.

### Requirements ###

* Visual Studio 2015 .sln file compatible IDE
* System with WPF library installed